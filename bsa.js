import Chat from './src/components/Chat';
import rootReducer from './src/redux/reducers/rootReducer';

export default {
    Chat,
    rootReducer,
};