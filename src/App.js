import './App.css';
import Chat from './components/Chat';

const chatMessages = process.env.REACT_APP_API_URL;

function App() {
  return (
    <div className="App">
      <Chat url = {chatMessages} />
    </div>
  );
}

export default App;
