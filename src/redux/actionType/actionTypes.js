export const SHOW_PRELOADER = "SHOW_PRELOADER";
export const HIDE_PRELOADER = "HIDE_PRELOADER";
export const SHOW_MODAL = "SHOW_MODAL";
export const HIDE_MODAL = "HIDE_MODAL";

export const GET_MESSAGES = "GET_MESSAGES";

export const ADD_MESSAGE = "ADD_MESSAGE";
export const DELETE_MESSAGE = "DELETE_MESSAGE";
export const EDIT_MESSAGE = "EDIT_MESSAGE";
