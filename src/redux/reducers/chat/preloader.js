import { SHOW_PRELOADER, HIDE_PRELOADER} from "../../actionType/actionTypes";

const reducer = (state = true, action) => {
  switch(action.type) {
    case SHOW_PRELOADER: {
      state = true;
      return state;
    }
    case HIDE_PRELOADER: {
      state = false;
      return state;
    }
    default:
      return state;
  }
}

export default reducer;