import { SHOW_MODAL, HIDE_MODAL } from "../../actionType/actionTypes";

const reducer = (state = false, action) => {
  switch(action.type) {
    case SHOW_MODAL: {
      state = true;
      return state;
    }
    case HIDE_MODAL: {
      state = false;
      return state;
    }
    default:
      return state;
  }
}

export default reducer;