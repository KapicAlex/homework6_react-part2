import {
  GET_MESSAGES,
  ADD_MESSAGE,
  DELETE_MESSAGE,
  EDIT_MESSAGE,
} from "../../actionType/actionTypes";

const reducer = (state = [], action) => {
  switch (action.type) {
    case GET_MESSAGES: {
      const messages = action.payload;
      state = messages;
      return state;
    }
    case ADD_MESSAGE: {
      const message = action.payload;
      state = [...state, message];
      return state;
    }
    case DELETE_MESSAGE: {
      const id = action.payload;
      const messagesNew = state.filter(
        (message) => message.id !== id
      );
      state = messagesNew;
      return state;
    }
    case EDIT_MESSAGE: {
      const messageNew = action.payload;
      const messagesNew = state.map((message) => {
        if (message.id === messageNew.id) {
          return messageNew;
        }
        return message;
      });
      state = messagesNew;
      return state;
    }
    default:
      return state;
  }
};

export default reducer;
