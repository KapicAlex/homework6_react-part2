import { combineReducers } from "redux";
import editReducer from "./edit";
import messageReducer from "./message"
import preloaderReducer from "./preloader"

const redusers = combineReducers({
  messages: messageReducer,
  editModal: editReducer,
  preloader: preloaderReducer,
});

export default redusers;