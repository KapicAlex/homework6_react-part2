import { combineReducers } from "redux";
import chatReducer from "./chat/reducersChat";

const rootReducer = combineReducers({
  chat: chatReducer,
});

export default rootReducer;