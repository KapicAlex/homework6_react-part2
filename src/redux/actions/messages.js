import {
  GET_MESSAGES,
  ADD_MESSAGE,
  DELETE_MESSAGE,
  EDIT_MESSAGE,
} from "../actionType/actionTypes";

export const getMessages = (messages) => ({
  type: GET_MESSAGES,
  payload: messages,
});

export const addMessage = (message) => ({
  type: ADD_MESSAGE,
  payload: message,
});

export const deleteMessage = (id) => ({
  type: DELETE_MESSAGE,
  payload: id,
});

export const editMessage = (message) => ({
  type: EDIT_MESSAGE,
  payload: message,
});
