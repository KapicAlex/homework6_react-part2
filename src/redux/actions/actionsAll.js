import * as editActions from "./edit";
import * as messagesActions from "./messages";
import * as preloaderActions from "./preloader";

export const actions = {...editActions, ...messagesActions, ...preloaderActions};