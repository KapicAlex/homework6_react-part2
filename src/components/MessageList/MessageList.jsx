import React from "react";
import moment from 'moment';
import { connect } from "react-redux";
import { actions } from "../../redux/actions/actionsAll";
import "./messageList.css";
import Message from "../Message/Message";
import OwnMesssage from "../OwnMessage/OwnMessage";
import Divider from "../Divider/Divider";

class MessageList extends React.Component {

  render() {
    const userId = this.props.userId;
    const editMessage = this.props.edit;

    return (
      <div className="message-list">

        {this.props.messages.map((message, idx) => {
          let temp = [];
          const prevDate = idx === 0 ? 1 : this.props.messages[idx-1].createdAt;
          const currentDate = message.createdAt;

          if(message.userId === userId) {

            if(prevDate && moment(prevDate).calendar() !== moment(currentDate).calendar()) {
              temp.push(<Divider key={message.createdAt} date={message.createdAt} />);
            }
            temp.push(<OwnMesssage key={message.id} message={message} edit={editMessage} />);
            return temp
          }

          if(prevDate && moment(prevDate).calendar() !== moment(currentDate).calendar()){
            temp.push(<Divider key={message.createdAt} date={message.createdAt} />)
          }
          
          temp.push(<Message key={message.id} message={message} />);
          return temp
        })}

      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.chat.messages,
  }
};

const mapDispatchToProps = {
  ...actions,
}

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);