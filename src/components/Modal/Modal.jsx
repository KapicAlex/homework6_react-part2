import React from "react";
import { connect } from "react-redux";
import { actions } from "../../redux/actions/actionsAll";
import "./modal.css";

class Modal extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      messageText: this.props.message ? this.props.message.text : "",
    }

    this.onChange=this.onChange.bind(this);
    this.editMessage=this.editMessage.bind(this);
    this.close=this.close.bind(this);
  }

  onChange(e) {
    const value = e.target.value;
    this.setState({
      messageText: value,
    });
  }

  close() {
    this.setState({messageText: ""});
    this.props.edit(this.props.message);
    this.props.hideModal();
  }

  editMessage(e) {
    e.preventDefault();

    if(this.state.messageText) {
      const messageNew = {
        ...this.props.message,
        text: this.state.messageText,
        editedAt: Date.now(),
      }
      this.props.edit(messageNew);
    }else{
      this.props.edit(this.props.message);
    }

    this.props.hideModal();
  }

  componentDidUpdate(prevProps) {
      if (prevProps.message !== this.props.message && !prevProps.message) {
        this.setState({messageText: this.props.message.text});
      }
    }

  render() {
    return(
      <div className={"edit-message-modal" + (this.props.modalShow ? " modal-shown" : "")}>
        <div className="modal-main">
          <div className="edit-message-title">Edit Message</div>
          <form onSubmit={this.editMessage} className="edit-message-form">
            <textarea name="message"
                    cols="30" rows="10"
                    className="edit-message-input"
                    value={this.state.messageText}
                    onChange={this.onChange}></textarea>
            <div className="edit-mesasge-conttrols">
              <button className="edit-message-button" type="submit">Edit</button>
              <button className="edit-message-close" type="button" onClick={this.close}>Close</button>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    modalShow: state.chat.editModal,
  }
};

const mapDispatchToProps = {
  ...actions,
}

export default connect(mapStateToProps, mapDispatchToProps)(Modal);