import React from "react";
import moment from 'moment';
import { connect } from "react-redux";
import { actions } from "../redux/actions/actionsAll";
import Header from "./Header/Header";
import Preloader from "./Preloader/Preloader";
import MessageList from "./MessageList/MessageList";
import MessageInput from "./MessageInput/MessageInput";
import Modal from "./Modal/Modal";
import "./chat.css";

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ownName: "Binary Academy Guru",
      ownId: "SUPERADMIN",
      avatar: "https://avatarfiles.alphacoders.com/238/238245.jpg",
      needEdit: false,
    };

    this.editMessage=this.editMessage.bind(this);
    this.completeEdit=this.completeEdit.bind(this);
    this.editLastMessage=this.editLastMessage.bind(this);
  }

  getMainInfo(messagesInfo) {
    const allUserId = messagesInfo.map(message => message.userId);
    const allUsers = new Set(allUserId);
    const lastMessageDate =  messagesInfo[messagesInfo.length - 1]?.createdAt;
    return {
      usersSum: allUsers.size,
      allMessages: allUserId.length,
      lastMessageDate: moment(lastMessageDate).format("DD.MM.YYYY HH:mm"),
    }
  }

  editMessage(message) {
    this.props.showModal();
    this.setState({...this.state, needEdit: message});
  }

  completeEdit(messageNew) {
    this.props.editMessage(messageNew);
    this.setState({...this.state, needEdit: false});
  }

  editLastMessage(e) {
    if(e.code === "ArrowUp" && !this.props.preloader && !this.props.editModal) {
      const ownMessages = this.props.messages.filter(message => message.userId === this.state.ownId);
      const lastMessage = ownMessages.pop();
      this.editMessage(lastMessage);
    }
  }
  
  async componentDidMount() {
    await fetch(this.props.url)
            .then(response => response.json())
            .then(data => {
              this.props.getMessages(data);
              this.props.hidePreloader()
            })
            .catch(err => console.log(err));
  }

  render() {
    return (
      <main className="chat" onKeyUp={this.editLastMessage}>
        <Preloader />
        <Header data = {this.getMainInfo(this.props.messages)}/>
        <MessageList userId = {this.state.ownId}
                    edit={this.editMessage} />
        <MessageInput data = {this.state} />
        <Modal message={this.state.needEdit}
                edit={this.completeEdit} />
      </main>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    messages: state.chat.messages,
    preloader: state.chat.preloader,
    editModal: state.chat.editModal
  }
};

const mapDispatchToProps = {
  ...actions,
}

export default connect(mapStateToProps, mapDispatchToProps)(Chat);