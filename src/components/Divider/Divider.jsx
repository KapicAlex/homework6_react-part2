import React from "react";
import moment from 'moment';
import "./divider.css"

class Divider extends React.Component {

  render() {

    const oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
    const firstDate = new Date(Date.now());
    const secondDate = new Date(this.props.date);
    const diffDays = Math.round(Math.abs((firstDate - secondDate) / oneDay));

    let correctDate = moment(this.props.date).calendar({
      sameDay: '[Today]',
      nextDay: '[Tomorrow]',
      lastDay: '[Yesterday]',
    })
    if(diffDays > 2) {
      correctDate = moment(this.props.date).format('dddd, DD MMMM');
    }

    return (
      <div className="messages-divider" >
          <div className="messages-divider-date">{correctDate}</div>
      </div>
    )
  }
}

export default Divider;