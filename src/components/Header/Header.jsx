import React from "react";
import "./header.css"

class Header extends React.Component {
  render() {
    const {usersSum, allMessages, lastMessageDate} = this.props.data;
    
    return (
      <div className="header">
        <h1 className="header-title">My chat</h1>
        <div><span className="header-users-count">{usersSum}</span>&nbsp;participants</div>
        <div><span className="header-messages-count">{allMessages}</span>&nbsp;messages</div>
        <div>last message at&nbsp;<span className="header-last-message-date">{lastMessageDate}</span></div>
      </div>
    )
  }
}

export default Header;