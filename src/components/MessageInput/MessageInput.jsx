import React from "react";
import { v4 as uuidv4 } from 'uuid';
import { connect } from "react-redux";
import { actions } from "../../redux/actions/actionsAll";
import "./messageInput.css";

class MessageInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messageText: "",
    }
    this.onChange=this.onChange.bind(this);
    this.addText=this.addText.bind(this);
  }

  onChange(e) {
    const value = e.target.value;
    this.setState({
      messageText: value,
    });
  }

  addText(e) {
    e.preventDefault();

    if(this.state.messageText) {
      const message = {
        id: uuidv4(),
        userId: this.props.data.ownId,
        avatar: this.props.data.avatar,
        user: this.props.data.ownName,
        text: this.state.messageText,
        createdAt: Date.now(),
        editedAt: "",
      };
      this.props.addMessage(message);
      this.setState({
        messageText: "",
      })
    }
  }

  render() {
    return (
      <div className="message-input" >
        <form onSubmit={this.addText}>
          <input type="text"
            className="message-input-text"
            placeholder="Enter your message"
            value={this.state.messageText}
            onChange={this.onChange} />
          <button className="message-input-button"
            type="submit">Send</button>
        </form>
      </div>
    )
  }
}

const mapDispatchToProps = {
  ...actions,
}

export default connect(null, mapDispatchToProps)(MessageInput);