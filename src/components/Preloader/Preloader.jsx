import React from "react";
import { CircularProgress } from "@material-ui/core";
import { connect } from "react-redux";
import { actions } from "../../redux/actions/actionsAll";
import "./preloader.css"

class Preloader extends React.Component {
  render() {
    return (
      <div className="preloader" style={{display: this.props.preloader ? "block" : "none"}}>
        <CircularProgress size="200px" />
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    preloader: state.chat.preloader,
  }
};

const mapDispatchToProps = {
  ...actions,
}

export default connect(mapStateToProps, mapDispatchToProps)(Preloader);